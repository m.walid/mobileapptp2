package fr.uavignon.ceri.tp2.view;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class ListViewModel extends AndroidViewModel {
    private BookRepository repository;
    private LiveData<List<Book>> allBooks;

    public ListViewModel(@NonNull Application application) {
        super(application);
        repository=new BookRepository(application);
        allBooks=repository.getAllBooks();
    }

    public LiveData<List<Book>> getAllBooks(){
        return allBooks;
    }
    public void insertBook(Book book){
        repository.insertBook(book);
    }
    public void getBook(long id){
        repository.getBook(id);

    }

    public void deleteBook(long id){
        repository.deletedBook(id);

    }

    public void updateBook(Book book){
        repository.updateBook(book);
    }
}
