package fr.uavignon.ceri.tp2.data;

import androidx.lifecycle.LiveData;
import androidx.room.*;

import java.util.List;

@Dao
public interface BookDao {
    @Update(onConflict = OnConflictStrategy.REPLACE)
    void updateBook(Book book);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBook(Book book);

    @Query("select * from books where bookId =:id")
    List<Book> getBook(long id);

    @Query("DELETE from books where bookId=:id")
    void deletedBook(long id);

    @Query("Select * from books;")
    LiveData<List<Book>> getAllBooks();

    @Query("DELETE FROM books")
    void deletedAllBooks();
}
