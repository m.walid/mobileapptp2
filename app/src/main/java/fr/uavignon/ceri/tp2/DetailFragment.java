package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.view.DetailViewModel;
import fr.uavignon.ceri.tp2.view.ListViewModel;

public class DetailFragment extends Fragment {
    private DetailViewModel viewModel;
    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel=new ViewModelProvider(this).get(DetailViewModel.class);
        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        Book book = Book.books[(int)args.getBookNum()];

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        viewModel.getBook((int)args.getBookNum());

        viewModel.getSelectedBook().observe(getViewLifecycleOwner(),
                new Observer<List<Book>>() {
                    @Override
                    public void onChanged(@Nullable final List<Book> books) {

                        if (books.size() > 0) {
                            textTitle = (EditText) view.findViewById(R.id.nameBook);
                            textAuthors = (EditText) view.findViewById(R.id.editAuthors);
                            textYear = (EditText) view.findViewById(R.id.editYear);
                            textGenres = (EditText) view.findViewById(R.id.editGenres);
                            textPublisher = (EditText) view.findViewById(R.id.editPublisher);
                            textTitle.setText(books.get(0).getTitle());
                            textAuthors.setText(books.get(0).getAuthors());
                            textYear.setText(books.get(0).getYear());
                            textGenres.setText(books.get(0).getGenres());
                            textPublisher.setText(books.get(0).getPublisher());
                        }
                    }
                });

        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //textTitle, textAuthors, textYear, textGenres, textPublisher;

                String title=textTitle.getText().toString();
                String author=textAuthors.getText().toString();
                String year=textYear.getText().toString();
                String genre=textGenres.getText().toString();
                String publisher=textPublisher.getText().toString();
                if(!title.equals("") && !author.equals("") && !year.equals("") &&
                        !genre.equals("") && !publisher.equals("")){
                    book.setId(args.getBookNum());
                    book.setTitle(title);
                    book.setAuthors(author);
                    book.setYear(year);
                    book.setGenres(genre);
                    book.setPublisher(publisher);
                    viewModel.updateBook(book);




                    Toast.makeText(view.getContext(), "success", Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(view.getContext(), "not all the fields are filled", Toast.LENGTH_SHORT).show();
                }


            }
        });


    }
}