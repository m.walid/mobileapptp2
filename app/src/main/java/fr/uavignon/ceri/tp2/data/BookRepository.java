package fr.uavignon.ceri.tp2.data;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;


import static fr.uavignon.ceri.tp2.data.BookRoomDatabase.databaseWriteExecutor;

public class BookRepository {

    private LiveData<List<Book>> allBooks;
    private BookDao bookDao;
    private MutableLiveData<List<Book>> selectedBook = new MutableLiveData<>();

    public BookRepository(Application application){
        BookRoomDatabase db=BookRoomDatabase.getDatabase(application);
        bookDao=db.bookDao();
        allBooks=bookDao.getAllBooks();
    }



    public void updateBook(Book book) {
        databaseWriteExecutor.execute(()->{
            bookDao.updateBook(book);
        });

    }


    public void insertBook(Book book) {
        databaseWriteExecutor.execute(()->{
            bookDao.insertBook(book);
        });
    }


    public void getBook(long id) {
        Future<List<Book>> fbooks = databaseWriteExecutor.submit(() -> {
            return bookDao.getBook(id);
        });
        try {
            selectedBook.setValue(fbooks.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void deletedAllBooks(){
        databaseWriteExecutor.execute(()->{
            bookDao.deletedAllBooks();
        });
    }

    public void deletedBook(long id) {
        databaseWriteExecutor.execute(() -> {
            bookDao.deletedBook(id);
        });
    }

    public MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }
    public LiveData<List<Book>> getAllBooks() {
        return allBooks;
    }
}
