package fr.uavignon.ceri.tp2.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Book.class},version = 1)
public abstract class BookRoomDatabase extends RoomDatabase {
    public abstract BookDao bookDao();
    private static volatile BookRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS=4;
    static final ExecutorService databaseWriteExecutor= Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    private static RoomDatabase.Callback sBookRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db){
            super.onCreate(db);
            databaseWriteExecutor.execute(()->{
                BookDao dao = INSTANCE.bookDao();
                dao.deletedAllBooks();
                for(Book newBook : Book.books) {
                    dao.insertBook(newBook);
                }
            });
        }
    };



    static BookRoomDatabase getDatabase(final Context context){

        if(INSTANCE == null){
            synchronized (BookRoomDatabase.class){
                if(INSTANCE == null){

                    INSTANCE= Room.databaseBuilder(context.getApplicationContext(),
                            BookRoomDatabase.class,"book_database").addCallback(sBookRoomDatabaseCallback).build();

                }
            }
        }
        return INSTANCE;
    }



    @NonNull
    @Override
    protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
        return null;
    }

    @NonNull
    @Override
    protected InvalidationTracker createInvalidationTracker() {
        return null;
    }

    @Override
    public void clearAllTables() {

    }


}
