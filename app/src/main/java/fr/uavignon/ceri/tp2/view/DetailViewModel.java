package fr.uavignon.ceri.tp2.view;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;
    private MutableLiveData<List<Book>> selectedBook;
    LiveData<List<Book>> allBooks;

    public DetailViewModel(Application application) {
        super(application);
        repository = new BookRepository(application);
        selectedBook = repository.getSelectedBook();
    }
    public void updateBook(Book book){
        repository.updateBook(book);

    }
    public LiveData<List<Book>> getAllBooks(){
        return repository.getAllBooks();
    }
    public MutableLiveData<List<Book>> getSelectedBook() {
        return selectedBook;
    }
    public void getBook(int id){repository.getBook(id);}
}
